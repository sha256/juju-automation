#!/bin/bash
homepath="${BASH_SOURCE%/*}"
reportpath=$homepath/jujustatus-reports
diff -b -w $reportpath/jujustatus_$(date -d "$date -1 days" "+%d%b%y") $reportpath/jujustatus_$(date +%d%b%y)
