homepath="${BASH_SOURCE%/*}"
reportpath=$homepath/jujustatus-reports
export PATH=
find $reportpath -name "jujustatus_*" -type f -mtime +30 -exec rm -f {} \;
juju status > $reportpath/jujustatus_`date +%d%b%y`
