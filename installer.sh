#!/bin/bash
jujuenvpath=$(env | grep PATH)
homepath="${BASH_SOURCE%/*}"
reportpath=$homepath/jujustatus-reports
chmod +x $homepath/*.sh
mkdir -p $reportpath
sed -i "s~PATH=~$jujuenvpath~g" $homepath/generate.sh
crontab -l > cronfile
echo "00 8 * * 1-5 $homepath/generate.sh" >> cronfile
crontab cronfile
rm cronfile
rm -- "$0"
